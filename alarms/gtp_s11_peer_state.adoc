---
title: "GTP S11 Peer State"
description: "Обнаружены GTP S11-соединения, не активные в течение короткого времени"
weight: 30
type: docs
---
[[gtp-s11-peer-state]]
== {app} GTP S11 Peer State
:alarm-name-interlude: Полное имя аварии:

{alarm-name-interlude} {app-service}.<NodeName>.{short-app}.GtpS11

[options="header",cols="1,8,8"]
|===
|Состояние |Описание |Передаваемые поля

|WARN
|Одно или несколько GTP S11-подключений на узле {app} неактивны {short-period}
.2+a|t1 -- Флаг наличия неактивных интерфейсов GTP S11 +
t2 -- Флаг неактивности всех интерфейсов GTP S11

|ERROR
|Все GTP S11-подключения на узле {app} неактивны {short-period}

|===

.Причины
* Cause_1;
* Cause_N;

.Действия
. {open-graph} {app-metrics-graph}::{gtp-s11-graph}::S11 State, отражающий состояние подключений по интерфейсу S11.

или

. Отправить API-запрос *_{api-gtp-s11-peers}_* для получения информации о состоянии подключений по интерфейсу S11.

или

. Выполнить cli-команду *_{cli-gtp-s11-state}_* для получения информации о состоянии подключений по интерфейсу S11.
. Определить неактивные узлы и время разрывов.
. {check-log} *_/usr/protei/{app-folder}/logs/gtp_c_warning.log_* {add-info}.
. Проверить состояния соединений с неактивными узлами командой `ping`.