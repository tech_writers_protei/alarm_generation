---
title: "High Load Average"
description: "Нагрузка на каждый процессор слишком велика"
weight: 30
type: docs
---
[[high-load-average]]
== {app} High Load Average
:alarm-name-interlude: Полное имя аварии:

{alarm-name-interlude} {app-service}.<NodeName>.LoadAverage

[options="header",cols="1,8,8"]
|===
|Состояние |Описание |Передаваемые поля

|WARN
|Нагрузка на каждый процессор превышает установленное значение {short-period}
.2+a|t1 -- Средняя нагрузка на каждый узел +
t2 -- Количество процессоров

|ERROR
|Нагрузка на каждый процессор превышает установленное значение в {load-average-coeff} раза {short-period}

|===

.Причины
* Cause_1;
* Cause_N;

Действия - FIXME