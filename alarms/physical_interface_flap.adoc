---
title: "Physical Interface Flap"
description: "Обнаружены физические интерфейсы, изменившие свое состояние несколько раз в течение короткого времени"
weight: 30
type: docs
---
[[physical-interface-flap]]
== {app} Physical Interface Flap
:alarm-name-interlude: Полное имя аварии:

{alarm-name-interlude} {app-service}.<NodeName>.{short-app}.Ph_int_Flap

[options="header",cols="1,8,8"]
|===
|Состояние |Описание |Передаваемые поля

|WARN
|Один или несколько физических интерфейсов изменили свое состояние более {flap-warn} раз {short-period}
.2+a|t1 -- Количество изменений состояния физических интерфейсов

|ERROR
|Один или несколько физических интерфейсов изменили свое состояние более {flap-err} раз {short-period}

|===

.Причины
* Cause_1;
* Cause_N;

Действия - FIXME