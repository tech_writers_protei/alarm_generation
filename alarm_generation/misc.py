# -*- coding: utf-8 -*-
from collections import deque
from pathlib import Path
from textwrap import dedent

from yaml import full_load

_FRONT_MATTER: str = dedent("""\
    ---
    title: "{title}"
    description: "{description}"
    weight: 30
    type: docs
    ---
    """)


_name_dict: dict[str, str] = {
    "Sgs": "SGs",
    "Sgsap": "SGsAP",
    "Gngp": "GnGp",
    "Irat": "iRAT"
}

_upper: tuple[str, ...] = (
    "Bfd", "Bgp", "Ha", "Rss", "Cpu", "Dns", "Gtp", "Ip", "Api", "Http", "Mo", "Mt", "Csfb", "Cs", "Ps", "Sms",
    "Pdn", "Lte", "Ho"
)


def multiple_replace(line: str):
    for k, v in _name_dict.items():
        if k in line:
            line = line.replace(k, v)

    for el in _upper:
        line = line.replace(el, el.upper())

    return line


def crlf_to_lf(path: str | Path):
    with open(path, "rb") as f:
        _ = f.read()

    with open(path, "wb") as f:
        f.write(_.replace(b"\r\n", b"\n"))


class DescriptionStorage(dict):
    def __init__(self):
        with open("descriptions.yaml", "r") as f:
            descriptions: dict[str, str] = full_load(f)
        super().__init__(**descriptions)


description_storage: DescriptionStorage = DescriptionStorage()


class AlarmStorage(dict):
    def __init__(self):
        with open("alarms.yaml", "r") as f:
            descriptions: dict[str, str] = full_load(f)
        super().__init__(**descriptions)

    def get_alarms(self, item: str) -> list[str]:
        return [alarm.replace(" ", "_").lower() for alarm in self[item]]


alarm_storage: AlarmStorage = AlarmStorage()


class FileManager:
    def __init__(self, path: str | Path | None = None, project_name: str = "%ProjectName%"):
        if path is None:
            path: Path = Path(__file__)

        self._path: Path = Path(path).resolve()
        self._content: deque[str] = deque()
        self._project_name: str = project_name

    def __str__(self):
        return "".join(self._content)

    def read(self):
        with open(self._path, "r", encoding="cp1251", errors="ignore") as f:
            self._content = deque(f.readlines())

    def write(self):
        with open(self._path, "w", encoding="cp1251") as f:
            f.write(str(self))

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, value):
        self._path = value

    def add_front_matter(self):
        title: str = multiple_replace(self._path.stem.title()).replace("_", " ")
        description: str = description_storage.get(title)
        self._content.appendleft(_FRONT_MATTER.format(title=title, description=description))

    def clear(self):
        self._path = None
        self._content = None


file_manager: FileManager = FileManager()


def generate_alarms(project_name: str):
    files: list[str] = alarm_storage.get_alarms(project_name)
    for file in files:
        Path(__file__).parent.parent.joinpath("alarms").joinpath(f"{file}.adoc")


def main():
    for file in Path(__file__).parent.parent.joinpath("alarms").iterdir():
        file_manager.clear()
        file_manager.path = file
        file_manager.read()
        file_manager.add_front_matter()
        file_manager.write()
        crlf_to_lf(file)


if __name__ == '__main__':
    main()
