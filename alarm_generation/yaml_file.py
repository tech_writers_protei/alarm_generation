# -*- coding: utf-8 -*-
from pathlib import Path
from typing import Any, Iterable, Mapping, NamedTuple, Sequence

from loguru import logger
from yaml import full_load


def title_to_snake(line: str):
    return line.lower().replace(" ", "_")


class NodeAlarms(NamedTuple):
    name: str
    general: list[str] = []
    specified: list[str] = []

    def get_files(self, section: str):
        try:
            _alarms: list[str] = getattr(self, section.title())
            return [f"{title_to_snake(_)}.adoc" for _ in _alarms]

        except AttributeError:
            logger.error(f"Missing section: {section.title()}")
            return

        except OSError as e:
            logger.error(f"{e.__class__.__name__}: {e.strerror}")
            raise

    def add_alarm(self, section: str, *, alarm: str = None, alarms: Iterable[str] = None):
        try:
            _alarms: list[str] = getattr(self, section.title())

            if alarm is not None:
                _alarms.append(alarm)
                logger.info(f"Alarm {alarm} has been added")

            if alarms is not None:
                _alarms.extend(alarms)
                _str_alarms: str = ", ".join(alarms)
                logger.info(f"Alarms {_str_alarms} has been added")

        except AttributeError:
            logger.error(f"Missing section: {section.title()}")
            return

        except OSError as e:
            logger.error(f"{e.__class__.__name__}: {e.strerror}")
            raise


class YAMLFile:
    def __init__(self, path: str | Path):
        self._path: Path = Path(path).resolve()
        self._content: dict[str, dict[str, list[str]]] = dict()

    def read(self):
        with open(self._path, "r", encoding="cp1251") as f:
            _: dict[str, dict[str, list[str]]] = full_load(f)

        for key, value in _.items():
            self._content[key] = dict()

            for k, v in value.items():
                for _v in v:
                    self._content[key][k].append(_v)

    def __getitem__(self, item):
        if isinstance(item, str):
            if item in self._content.keys():
                return self._content.get(item)
            else:
                logger.error(f"Invalid key {item}")
                raise KeyError
        else:
            logger.error(f"Invalid type {type(item)} of the key {item}")
            raise TypeError

    get = __getitem__

    def get_content(self, key: str | Sequence[str], content: Mapping[str, Any] = None):
        if content is None:
            content = self._content

        if isinstance(key, str):
            if key in content.keys():
                return content.get(key)

            else:
                logger.error(f"Invalid key {key}")
                raise KeyError

        else:
            return self.get_content(key[1:], content.get(key[0]))


class AlarmsFile(YAMLFile):
    def __init__(self):
        path: str = "../sources/alarms.yaml"
        super().__init__(path)
        self._node_alarms: list[NodeAlarms] = []

    def __add__(self, other):
        if isinstance(other, NodeAlarms):
            self._node_alarms.append(other)
        else:
            logger.error()

    @property
    def node_alarms(self):
        return self._node_alarms

    @node_alarms.setter
    def node_alarms(self, value):
        self._node_alarms = value

    def set_node_alarms(self):
        for k, v in self._content.items():
            node_alarm: NodeAlarms = NodeAlarms(k)

            general: list[str] = self.get_content((k, "general"))
            node_alarm.add_alarm("general", alarms=general)

            specified: list[str] = self.get_content((k, "specified"))
            node_alarm.add_alarm("specified", alarms=specified)

            self + node_alarm
